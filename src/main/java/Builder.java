public interface Builder {

    void setStrategy (SortStrategy type);
    void setPivot (int pivot);
}
