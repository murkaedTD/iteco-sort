public class Insert extends SortStrategy {

    public int[] arr;
    public int pivot;

    public void sort(int[] arr, int pivot) {
        int t;
        int i, j;
        for (i = pivot; i < arr.length; i++) {
            t = arr[i];
            for (j = i - 1; j >= pivot - 1 && arr[j] > t; j--) {
                arr[j + 1] = arr[j];
            }
            arr[j + 1] = t;
        }
    }
}
