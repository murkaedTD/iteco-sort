public class SorterBuilder implements Builder {

    private SortStrategy type;
    private int pivot;


    public static class Builder{

    }

    public void setStrategy(SortStrategy type) {
        this.type = type;
    }

    public void setPivot(int pivot) {
        this.pivot = pivot;
    }

    public SorterBuilder(SortStrategy type, int pivot) {
        this.type = type;
        this.pivot = pivot;
    }
}
